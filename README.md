bdayfind
=======================

# Description
Calculate special birthyears and ages, with mathematical properties.

# Usage
```
$ bdayfind > results.txt
```

# Source
Based on the idea of a [Numberphile YouTube video](https://www.youtube.com/watch?v=99stb2mzspI).

# License
```
        DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
                    Version 2, December 2004 

 Copyright (C) 2004 Sam Hocevar <sam@hocevar.net> 

 Everyone is permitted to copy and distribute verbatim or modified 
 copies of this license document, and changing it is allowed as long 
 as the name is changed. 

            DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE 
   TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION 

  0. You just DO WHAT THE FUCK YOU WANT TO.
```
